;(function ( $, window, document, undefined ) {
    var pluginName = "slickPanel",
        defaults = {
            color: "#000000",
            width: 300,
            gutter: 20,
            mode: "overlay", // overlay, shove
            overlay: {enabled: true, close: true, opacity: 0.4},
            opacity: 1,
            direction: "left",
            slideDuration: 400,
            opened: $.noop,
            closed: $.noop,
            wrapperClass: "slickPanel-wrapper",
            closeClass: "slickPanel-close",
            closeHtml: "&#10006;",
            closeHover: null
        };

    function SlickPanel ( element, options ) {
        this.element = element;
        defaults.overlay = $.extend({}, defaults.overlay, options.overlay);
        delete options.overlay;
        this.settings = $.extend( {}, defaults, options );
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

	SlickPanel.prototype = {
        init: function () {
            var pl = this,
                ops = this.settings;

            if (ops.overlay.enabled) {
                pl.overlay = $("<div class='slickPanel-overlay'/>").css("opacity", ops.overlay.opacity);
                $('body').append(pl.overlay);

                if (ops.overlay.close) {
                    pl.overlay.bind('click', function(){
                        pl.close();
                    });
                }
            }

            pl.wrapper = $(pl.element).wrap("<div class='" + ops.wrapperClass + "'/>").parent();
            pl.wrapper
                .css("background", ops.color)
                .css(ops.direction, - (ops.width + (ops.gutter * 2)))
                .css("padding", ops.gutter); // user set gutter default is 5px;

            pl.closeButton = $("<span class='" + ops.closeClass + "'/>");
            pl.closeButton.html(ops.closeHtml)
                .css(ops.direction, ops.width + ops.gutter - 3)
                .bind('click', function(){
                    pl.close();
                });
            pl.wrapper.append(pl.closeButton);

            if(ops.direction === "left" || ops.direction === "right"){
                pl.wrapper.css("top", 0).css("bottom", 0).css("width", ops.width);
                pl.closeButton.css("top", "50%").css("height", "100%");
            } else {
                pl.wrapper.css("left", 0).css("right", 0).css("height", ops.width);
                pl.closeButton.css("left", "50%").css("width", "100%");
            }

            // Bind events
            $(pl).on('panelOpened', function(e){
                ops.opened(e);
            });

            $(pl).on('panelClosed', function(e){
                ops.closed(e);
            });

        },
        _animatePanelOpen: function () {
            var animOptions = {},
                pl = this,
                ops = this.settings;
                animOptions[ops.direction] = 0;

            $(this.wrapper).animate(animOptions, {
                duration: ops.slideDuration,
                step: function( now, fx ){
                    // If this is shove then push content over
                    if(ops.mode === "shove"){
                        //self._bodyWrapper.css("margin-" + ops.direction, now + (ops.width + ops.gutter * 2));
                    }
                },
                complete: function () {
                    $(pl).trigger('panelOpened');
                }
            });
        },
        _animatePanelClose: function (callback) {
            var pl = this,
                ops = this.settings,
                animOptions = {};

            animOptions[ops.direction] = - (ops.width + (ops.gutter * 2));

            pl.wrapper.animate(animOptions, {
                duration: ops.slideDuration,
                step: function( now, fx ){
                    // If this is shove then push content over
                    if(ops.mode === "shove"){
                        //self._bodyWrapper.css("margin-" + direction, now + (self.options.width + self.options.gutter * 2));
                    }
                },
                complete: callback
            });
        },
        open: function () {
            var pl = this,
                ops = this.settings;

            pl.wrapper.css("display", "block");
            if(ops.overlay.enabled) {
                this.overlay.fadeIn('fast', function() {
                    pl._animatePanelOpen();
                });
            } else {
                pl._animatePanelOpen();
            }
        },
        close: function () {
            var pl = this,
                ops = this.settings;
            if (ops.overlay.enabled) {
                pl._animatePanelClose(function() {
                    pl.overlay.fadeOut('fast', function() {
                        pl.wrapper.css("display", "none");
                        $(pl).trigger('panelClosed');
                    });
                });
            } else {
                pl._animatePanelClose(function() {
                    pl.wrapper.hide();
                    $(pl).trigger('panelClosed');
                });
            }
        }
    };

    $.fn[ pluginName ] = function ( options ) {
        var args = arguments;
        if(options === undefined || typeof options === 'object') {
            return this.each(function() {
                if ( !$.data( this, "plugin_" + pluginName ) ) {
                        $.data( this, "plugin_" + pluginName, new SlickPanel( this, options ) );
                }
            });
        } else if (typeof options === 'string' && options[0] != '_' && options != 'init') {
            var returns;
            this.each(function () {
                var instance = $.data(this, 'plugin_' + pluginName);
                if (instance instanceof SlickPanel && typeof instance[options] === 'function') {
                    returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1));
                }
                if (options === 'destroy') {
                    $.data(this, 'plugin_' + pluginName, null);
                }
            });
            return returns !== undefined ? returns : this;
        }
    };

})( jQuery, window, document );