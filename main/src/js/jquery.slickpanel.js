/*!
 * SlickPanel a simple jQuery sliding panel plugin.
 * Author: @fhawkes
 * Licensed under the MIT license
 */

;(function ( $, window, document, undefined ) {

    $.widget( "fliz.slickPanel" , {

        //Options to be used as defaults
        options: {
            color: "#00000",
            width: 300,
            gutter: 5,
            mode: "overlay", // overlay, shove
            overlay: {enabled: true, close: true, opacity: 0.4}, // enable: whether to use an overlay
            opacity: 1,                           // close: whether to close panel when overlay is clicked
            opened: $.noop,
            closed: $.noop,
            direction: "left",
            slideDuration: 400,
            anchor: "body",
            wrapperClass: "slickPanel-wrapper",
            wrapperCss: {},
            closeClass: "slickPanel-close",
            closeCss: {},
            closeHtml: "&#10006;",
            closeHover: null
        },

        _create: function () {
            var self = this;
            var wrapperClass = this.options.wrapperClass;
            self._top = $(self.options.anchor).position().top; // this may need to get changed to offset
            if(self.options.overlay.enabled){
                self._overlay = $('<div class="slickPanel-overlay"></div>')
                    .css("background", "#000000")
                    .css("position", "fixed")
                    .css("top", 0)
                    .css("bottom", 0)
                    .css("left", 0)
                    .css("right", 0)
                    .css("z-index", 9998)
                    .css("display", "none")
                    .css("opacity", self.options.overlay.opacity);

                if(self.options.overlay.enabled){
                    $(self.options.anchor).append(self._overlay);
                }

                if(self.options.overlay.close){
                                     self._overlay.click(function(){
                                         self.close();
                                     });
                                 }
            }

            var direction = self.options.direction;
            if(direction === "left" || direction === "right"){
                // Create and style panel wrapper
                self._panelContainer = $('<div class="' + wrapperClass + '"></div>')
                            .css("background", self._convertHex(self.options.color, self.options.opacity))
                            .css("width", self.options.width)
                            .css("top", self._top)
                            .css("z-index", 9999)
                            .css("display", "none")
                            .css("padding-top", "30px")
                            .css("padding-left", self.options.gutter)
                            .css("padding-right", self.options.gutter)
                            .css(self.options.direction, - (self.options.width + (self.options.gutter * 2))) // STILL NEED TO DO THIS
                            .css("bottom", 0)
                            .css("position", "fixed")
                            .css("white-space", "nowrap");
            } else {
                // Create and style panel wrapper
                self._panelContainer = $('<div class="' + wrapperClass + '"></div>')
                            .css("background", self._convertHex(self.options.color, self.options.opacity))
                            .css("height", self.options.width)
                            .css("z-index", 9999)
                            .css("display", "none")
                            .css("padding", self.options.gutter)
                            .css(self.options.direction, - (self.options.width + (self.options.gutter * 2)))
                            .css("left", 0)
                            .css("right", 0)
                            .css("position", "fixed")
                            .css("white-space", "nowrap");
            }

            // Add user defined css styles to panel wrapper
            for (var key in self.options.wrapperCss){
                if( self.options.wrapperCss.hasOwnProperty(key) ){
                    self._panelContainer.css(key, self.options.closeCss[key]);
                }
            }


            // Wrap panel content in panel wrapper
            self.element.wrap(self._panelContainer);

            // Get this instance of the panel wrapper and store it since
            // $.wrap creates a copy of the DOM element first
            self._panelContainer = self.element.closest("." + wrapperClass);

            // Create and style close button (using default styles)

            if(direction === "left" || direction === "right"){
                self._closeButton = $('<a class="' + self.options.closeClass + '">' + self.options.closeHtml + '</a>')
                        .css(self._getCloseLocation(self.options.direction), 0)
                        .css("color", "#CCC")
                        .css("cursor", "pointer")
                        .css("margin", "0 1rem 1rem")
                        .css("text-decoration", "none")
                        .css("top", "0.65rem")
                        .css("position", "absolute")
                        .css("font-weight", "bold")
                        .css("font-size", "17px")
                        .css("outline","none")
                        .hover(
                            function () {
                                $(this).css("color", "#FF0000");
                              },
                              function () {
                                $(this).css("color", "#CCC");
                              }
                        )
                        .bind("click", function(){
                            self.close();
                        });
            } else {
                var closeHtml;

                if(self.options.closeHtml == "&#10006;") {
                    closeHtml = (direction == "top") ? "&#9650;" : "&#9660";
                } else {
                    closeHtml = self.options.closeHtml;
                }
                self._closeButton = $('<a class="' + self.options.closeClass + '">' + closeHtml + '</a>')
                        .css(self._getCloseLocation(self.options.direction), 0)
                        .css("color", "#CCC")
                        .css("cursor", "pointer")
                        .css("text-align", "center")
                        .css("width", "100%")
                        .css("text-decoration", "none")
                        .css("position", "absolute")
                        .css("font-weight", "bold")
                        .css("font-size", "17px")
                        .css("outline","none")
                        .css(self._getCloseLocation(direction), "0.65rem")
                        .hover(
                            function () {
                                $(this).css("color", "#FF0000");
                              },
                              function () {
                                $(this).css("color", "#CCC");
                              }
                        )
                        .bind("click", function(){
                            self.close();
                        });
            }

            if(self.options.closeHover != null){
                self._closeButton.hover(self.options.closeHover);
            }
            // Add user defined css styles to closeButton
            for (var key in self.options.closeCss){
                if( self.options.closeCss.hasOwnProperty(key) ){
                    self._closeButton.css(key, self.options.closeCss[key]);
                    if(key == "color"){
                        self._closeButton.hover(function () {
                            $(this).css("color", "#FF0000");
                        },
                        function () {
                            $(this).css("color", self.options.closeCss[key]);
                        });
                    }
                }
            }

            self._panelContainer.prepend(self._closeButton);

            if(self.options.mode === "shove"){
                if($(".slickPanel-body").length == 0){
                    self._bodyWrapper = $('<div class="slickPanel-body"></div>');
                    $("body").wrapInner(self._bodyWrapper);
                }

                self._bodyWrapper = $(".slickPanel-body");
                $(self.options.anchor).prepend(self._panelContainer);
            }

        }
    });

})( jQuery, window, document );